# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)


#Band

Band.create(name: 'The Band Perry', num_members: 3)
Band.create(name: 'Maroon 5', num_members: 5)
Band.create(name: 'Rascal Flatts', num_members: 4)
Band.create(name: 'Lady Antebellum', num_members: 3)
Band.create(name: 'Zac Brown Band', num_members: 4)

#Club

Club.create(name: 'Arena Theater', street_address: '7326 Southwest Fwy')
Club.create(name: 'Toyota Center', street_address: '1510 Polk St')

#Booking
Booking.create(band_id: 1, club_id: 1, fee: 25.99, date: '03-09-2014')
Booking.create(band_id: 1, club_id: 2, fee: 35.50, date: '02-01-2014')
Booking.create(band_id: 2, club_id: 1, fee: 45.99, date: '05-04-2014')
Booking.create(band_id: 2, club_id: 2, fee: 50.99, date: '03-02-2014')
Booking.create(band_id: 3, club_id: 1, fee: 80.99, date: '08-02-2014')
Booking.create(band_id: 3, club_id: 2, fee: 25.99, date: '03-05-2014')
Booking.create(band_id: 4, club_id: 1, fee: 45.50, date: '06-03-2014')
Booking.create(band_id: 4, club_id: 2, fee: 35.50, date: '20-05-2014')
Booking.create(band_id: 5, club_id: 1, fee: 55.50, date: '14-08-2014')
Booking.create(band_id: 5, club_id: 2, fee: 25.99, date: '06-04-2014')



