class Club < ActiveRecord::Base
  has_many :Bookings
  has_many :Bands, through: :Bookings
end
