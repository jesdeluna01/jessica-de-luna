class Booking < ActiveRecord::Base

 #so that the fee field is not left blank
  validates_presence_of :fee
  #so that the fee field is not left blank

  #so that the fee only accepts numbers greater than 0
  validates_numericality_of :fee, :greater_than_or_equal_to => 0
  #so that the fee only accepts numbers greater than o

  belongs_to :Band
  belongs_to :Club
end
