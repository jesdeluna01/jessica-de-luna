class Band < ActiveRecord::Base
  has_many :Bookings
  has_many :Clubs, through: :Bookings

end
